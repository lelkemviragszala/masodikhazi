/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vezerlo;


import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;


/**
 *
 * @author Administrator
 */
public class KepBeolvasas {
    
    private String kepEleres;

    public KepBeolvasas(String kepEleres) {
        this.kepEleres = kepEleres;
    }
    
    public List<ImageIcon> SzekKepekBeolvasasa() throws FileNotFoundException, IOException{
        List<ImageIcon> szekKepek=new ArrayList<>();
       for(int i=1;i<7;i++) {
       BufferedImage Kep =
        ImageIO.read(new FileInputStream("src/resources/szek"+i+".png"));
       szekKepek.add(new ImageIcon(Kep));
           //System.out.println(Kep.toString());
       }
       return szekKepek;
    }
}
