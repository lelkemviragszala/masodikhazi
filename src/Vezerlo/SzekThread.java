/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vezerlo;

import JPanel.SzekJPanel;
import java.awt.Graphics;
import java.awt.Image;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author Administrator
 */
public class SzekThread{
    
    private List<ImageIcon> SzekKepek;
    
    private int szamlalo;
    private SzekJPanel szekJPanel;
    private long miliszek;
    private boolean mukodik;
    private boolean keszASzek;

    public boolean isKeszASzek() {
        return keszASzek;
    }

    public boolean isSzekKep() {
        return (szekKep!=null);
    }

    public boolean isMukodik() {
        return mukodik;
    }

    public void setMukodik(boolean mukodik) {
        this.mukodik = mukodik;
    }
    
    private ImageIcon szekKep;
    
    int width;
    int height;
    int kepMeretX=100;
    int kepMeretY=200;

    public SzekThread(List<ImageIcon> SzekKepek, SzekJPanel szekJPanel1, long miliszek,int width, int height) {
        this.SzekKepek = SzekKepek;
        this.szamlalo = 0;
        this.szekJPanel = szekJPanel1;
        this.miliszek = miliszek;
        this.width=width;
        this.height=height;
        mukodik=false;
    }
    
    
    
    public synchronized void rajzolas(Graphics g){
        //if(szekKep!=null){
        Image szekKeptest=szekKep.getImage();
        int x=width-((kepMeretX+width)/2);
        int y=height-kepMeretY;
        g.drawImage(szekKeptest,x,y,100,200,null);
        //}
    }

    
    public void startSzek() {
        if(mukodik){
        new Thread(new Runnable() {
            //"Egyszerhívodik-e meg ha if(mukodik)-et elhagyom? Kell vele foglalkoznom?"
            @Override
            public void run() {
             boolean whileFuthat=true;           
       while(whileFuthat){
            try {
                System.out.println("Fut a SzekThread");
                szekKep=SzekKepek.get(szamlalo);
                szamlalo++;
                //szekJPanel.repaint();
                if((szamlalo)%SzekKepek.size()==0){
                    whileFuthat=false;
                    mukodik=false;
                };
                Thread.sleep(miliszek);
            } 
            catch (InterruptedException ex) {
                Logger.getLogger(SzekThread.class.getName()).log(Level.SEVERE, null, ex);
            }
       }
            }
        }).start();
        keszASzek=true;

    }
    }
    
}
