/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vezerlo;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author Administrator
 */
public class BoszorkanyThread extends Lakos {
    
    private ImageIcon gifIcon;//Image gif;
    private Image gif;
    private double kepY;
    private double kepX;
    private double deltaX;
    private double panelSzelesseg;
    private double panelMagassag;
    
    public boolean elhagyta(){
        System.out.println("kepX:"+kepX+"panelSzelesseg:"+panelSzelesseg);
        return kepX>panelSzelesseg;
    }
    
    public BoszorkanyThread(String nev,int szelesseg,int magassag) {
        super(nev);
        try {
            BufferedImage buffImage=ImageIO.read(new FileInputStream("src/resources/boszi.gif"));
            gifIcon=new ImageIcon(buffImage);//).getImage();
            gif=gifIcon.getImage();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BoszorkanyThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BoszorkanyThread.class.getName()).log(Level.SEVERE, null, ex);
        }
       this.panelSzelesseg=szelesseg;
       this.panelMagassag=magassag;
        kepX=this.panelSzelesseg;
        Random rand=new Random();
        kepY=(this.panelSzelesseg/2)-rand.nextDouble()*((this.panelSzelesseg/2)-gifIcon.getIconHeight());
        deltaX=rand.nextDouble()*20;
    }
    
    public void bosziRajzolas(Graphics g){
        g.drawImage(gif, (int)kepX, (int)kepY,50,50, null);
    }

    public void mozgat() {
        kepX+=deltaX;
    }
}
