/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vezerlo;

import JPanel.SzekJPanel;
import JPanel.VezerlesJPanel;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author Administrator
 */
public class Vezerlo {
    private static SzekJPanel szekJPanel;
    private VezerlesJPanel vezerlesJPanel;
    private String kepEleres="";
    private SzekThread szekThread;
    public static boolean klikkeltEgyet;

   
    
    public void vezerlesJPanelGameOver(){
        vezerlesJPanel.GameOver();
    }

    public Vezerlo(SzekJPanel szekJPanel1,VezerlesJPanel vezerlesJPanel1){        
        this.szekJPanel = szekJPanel1;
        this.vezerlesJPanel=vezerlesJPanel1;
        klikkeltEgyet=false;
        System.out.println("vezerlo-klikkelEgyet=false;");
        System.out.println("Vezérlő konstruktorát meghívta a 2.frame");
    }
    
    public static void start(){//nem tudom miért kell statikusnak lennie
        System.out.println("VezerlesJPanel meghívta Vezerlo start()-t");
        if(!klikkeltEgyet){
            System.out.println("vezerlo-!klikkeltEgyet");
        new Thread(new Runnable() {
            @Override
            public void run() {
                klikkeltEgyet=true;
                while(true){
                    szekJPanel.mukodtet();//ezt állandóan hívogatom ez meghívja a SzekJPanel szekThreadInditas()-t ez pedig a SzekThread startSzeket() ami mindig egy Thread konstruktorát->szekThread.setMukodik(true); által beállított SzekThread fieldjét a SzekThread startSzek while() ciklusában hammisra kell állítani? 
                    szekJPanel.repaint();
                    try {
                        Thread.sleep(200);//hogy ne pörögjön 50%-on a proci
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Vezerlo.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
            }
        }).start();
        }
    }
   

    /*public void rajzolas(Graphics g){
        szekThread.rajzolas(g);
    }

    public void frissit() {
        System.out.println("Vezerlo.frissit meg lett hívva");
        szekJPanel.repaint();
        System.out.println("vezerlo frissit");
    }*/
}