/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPanel;

import Vezerlo.BoszorkanyThread;
import Vezerlo.Lakos;
import Vezerlo.Vezerlo;
import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.ListModel;

/**
 *
 * @author Administrator
 */
public class VezerlesJPanel extends javax.swing.JPanel {
    

    private Vezerlo vezerlo;

    public void setVezerlo(Vezerlo vezerlo) {
        this.vezerlo = vezerlo;
    }
    //private 
    private DefaultListModel<Lakos> lakosok=new DefaultListModel<Lakos>() ;
    private DefaultListModel<BoszorkanyThread> boszik=new DefaultListModel<BoszorkanyThread>() ;
    private double xArany=0.7;
    private List<BoszorkanyThread> bosziLista;

    public List<BoszorkanyThread> getBosziLista() {
        return bosziLista;
    }
   
    public VezerlesJPanel() {
        initComponents();
        jList1.setModel(lakosok);
        jList2.setModel(boszik);
        jListfeltoltes();
        bosziLista=Collections.list(boszik.elements());
        //System.out.println("BosziLista mérete:"+bosziLista.size());
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        jButton1.setText("Start");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(jList1);

        jScrollPane2.setViewportView(jList2);

        jLabel1.setText("Lakosok");

        jLabel2.setText("Boszorkányok");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addComponent(jButton1))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(67, 67, 67)
                                .addComponent(jLabel1)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addComponent(jLabel2)
                .addContainerGap(56, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(14, 14, 14)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addGap(21, 21, 21))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       vezerlo.start();
       
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList<Lakos> jList1;
    private javax.swing.JList<BoszorkanyThread> jList2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables

    private void jListfeltoltes() {
        try {
                BufferedReader lakok=new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/resources/lakok.txt")));
                String sor="";
                while((sor=lakok.readLine())!=null){
                    Lakos lakos=new Lakos(sor);
                    lakosok.addElement(lakos);
                    Random rand=new Random();
                    if(rand.nextDouble()<xArany){
                        BoszorkanyThread boszi=new BoszorkanyThread(lakos.getNev(),SzekJPanel.SzekJPanelSzelesseg,SzekJPanel.SzekJPanelMagassag);
                        boszik.addElement(boszi);
                    }
                }
            } catch (IOException ex) {
            Logger.getLogger(VezerlesJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void GameOver() {
         for(Component c:getComponents()){
             c.setVisible(false);
         }
        try {
            BufferedImage buffImage=ImageIO.read(new FileInputStream("/src/resources/utso.jpg"));
            JLabel label=new JLabel();
        label.setIcon(new ImageIcon(buffImage));
        add(label);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VezerlesJPanel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VezerlesJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
