/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPanel;

import Vezerlo.BoszorkanyThread;
import Vezerlo.KepBeolvasas;
import Vezerlo.SzekThread;
import Vezerlo.Vezerlo;
import java.awt.Graphics;
import static java.awt.Image.SCALE_DEFAULT;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author Administrator
 */
public class SzekJPanel extends javax.swing.JPanel {

    private Vezerlo vezerlo;
    private SzekThread szekThread;
    private boolean raKlikkelt;
    private List<BoszorkanyThread> bosziLista;
    private int osszesBosziElhagyta;
    
    public static int SzekJPanelSzelesseg;
    public static int SzekJPanelMagassag;
    

    public void setBosziLista(List<BoszorkanyThread> bosziLista) {
        //System.out.println("SzekJPanel-setBosziLista Mehgívtam az második ablak konstruktorában");
        this.bosziLista = bosziLista;
    }
    
    
    public void setVezerlo(Vezerlo vezerlo){
        this.vezerlo=vezerlo;
    }
   
    public SzekJPanel() {
        //System.out.println("SzekJPanel szelessege:"+this.getWidth());
        initComponents();
        System.out.println("SzekJPanel szelessege:"+this.getBounds().width);
        System.out.println("SzekJPanel konstruktorát meghívta az AblakFrame");
        raKlikkelt=false;
        osszesBosziElhagyta=0;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        try {
            BufferedImage Kep =
                    ImageIO.read(new FileInputStream("src/resources/hatter.jpg"));
            g.drawImage(new ImageIcon(Kep).getImage().getScaledInstance(this.getWidth(), this.getHeight(), SCALE_DEFAULT),0, 0, this);
        } catch (Exception ex) {
            Logger.getLogger(SzekJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(szekThread!=null&&szekThread.isSzekKep()){//&&szekThread.isMukodik()){//ahhoz hogy a SzekThread SzekKep létezésekor fusson csak
            szekThread.rajzolas(g);
            //System.out.println("Panelig megy");
        }
        if(raKlikkelt){
          for(BoszorkanyThread boszi:bosziLista){
                boszi.bosziRajzolas(g);
            }  
        }
        
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();

        jButton1.setText("jButton1");

        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
       if(szekThread!=null && szekThread.isKeszASzek()&&!raKlikkelt){
           raKlikkelt=true;
       }
    }//GEN-LAST:event_formMouseClicked

     public void szekThreadInditas(){
        try {
            KepBeolvasas kepBeolvasas=new KepBeolvasas("");
            List<ImageIcon> szekKepek=kepBeolvasas.SzekKepekBeolvasasa();
            szekThread=new SzekThread(szekKepek,this,2000, this.getWidth(),this.getHeight());
            szekThread.setMukodik(true);
            szekThread.startSzek();
        } catch (IOException ex) {
            Logger.getLogger(Vezerlo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    // End of variables declaration//GEN-END:variables

    public void mukodtet() {
        SzekJPanelMagassag=this.getHeight();
        SzekJPanelSzelesseg=this.getWidth();
        if(szekThread==null){
        szekThreadInditas();
        }
        
        if(raKlikkelt){
            for(BoszorkanyThread boszi:bosziLista){
                boszi.mozgat();
                if(boszi.elhagyta()){
                    osszesBosziElhagyta++;
                    //System.out.println(osszesBosziElhagyta);
                }
                if(osszesBosziElhagyta==bosziLista.size()&&vezerlo!=null){
                    System.out.println("Elhagyták");
                    vezerlo.vezerlesJPanelGameOver();
                }
            }
        }
    }
}
